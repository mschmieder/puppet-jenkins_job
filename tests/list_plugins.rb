require 'erb'


require File.join(File.dirname(__FILE__), '../lib/puppet/jenkins', 'job.rb')

properties = { 
    'description' => "This is a multi scm job",
    'scm' => 
    [
        {
         'type' => 'git',
         'params' =>  {
            'url' => 'git@bitbucket.org:mschmieder/license.git',
            'branch' => 'master',
            'extensions' => {
                'type' => 'RelativeTargetDirectory',
                'params' => { 'relativeTargetDir' => 'license'}
                }
            },
        }
    ]
}

# require 'erb'
# git_version = "Rasmus"
# template_string = File.read(File.join(File.dirname(__FILE__), '../lib/templates', 'job_single_scm.xml.erb'))
# template = ERB.new template_string
# puts template.result # prints "My name is Rasmus"

# puts Puppet::Jenkins::Plugins::Git.version

puts Puppet::Jenkins::Plugins::available
puts Puppet::Jenkins::Job.create_xml(properties)