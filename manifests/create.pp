# FREESTYLE JOB CONFIGURATION

define jenkins_job::create ( 
  $job_template      = "",
  $user_options      = {},
  $description       = "",
  $axes              = {},
  $properties        = {},
  $scm               = {},
  $triggers          = {},
  $build_wrappers    = {},
  $builders          = {},
  $publishers        = {},
  $assignedNode      = {},
  $additional_params = {},
  $enabled           = 1,
  $ensure            = 'present')
{
  case $ensure {
    1       : { $disabled = false }
    0       : { $disbaled = true }
    default : { $disabled = false }
  }

  case $assignedNode {
    ''       : { $canRoam = true }
    {}       : { $canRoam = true }
    default  : { $canRoam = false }
  }

  $props = {
    'axes'           => $axes,
    'template'       => $job_template,
    'properties'     => $properties,
    'description'    => $description,
    'scm'            => $scm,
    'builders'       => $builders,
    'publishers'     => $publishers,
    'triggers'       => $triggers,
    'build_wrappers' => $build_wrappers,
    'params'         => merge($additional_params,{'assignedNode' => $assignedNode,'disabled' => $disabled, 'canRoam' => $canRoam }),
    'user_options'   => $user_options
  }

  jenkins::job { "${name}" : 
    config   => template_jenkins_job($props),
    jobname  => "${name}",
    enabled  => $enabled,
    ensure   => "${ensure}"
  }
}