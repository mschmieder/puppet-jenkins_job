require 'erb'
require 'json'
require "rexml/document"

require File.join(File.dirname(__FILE__), '../../../../jenkins/lib/puppet', 'jenkins.rb')
require File.join(File.dirname(__FILE__), '../../../../jenkins/lib/puppet/jenkins', 'plugins.rb')

class ::Hash
  def deep_merge(second)
    merger = proc { |key, v1, v2| Hash === v1 && Hash === v2 ? v1.merge(v2, &merger) : Array === v1 && Array === v2 ? v1 | v2 : [:undefined, nil, :nil].include?(v2) ? v1 : v2 }
    self.merge(second.to_h, &merger)
  end
end

module Puppet
  module Jenkins
    module Job
      def self.hash_to_xml(group,element_name,key,value)
        content = ''
        if value.is_a?(::Array)
          # TODO: key needs to be checked if it is a sub_class element or a type_wrapper
          content += "<"+key+">"
          value.each do |element|
            if element.is_a?(::Hash)
              element.each do |inner_key,inner_value|
                content += hash_to_xml(group,element_name,inner_key,inner_value)
              end
            else
              raise "inner array element is not a Has"
            end        
          end
          content += "</"+key+">"
        else
          # we need to check of the key is part of a specialized
          # class object of the type object
          already_processed=false
          if value.is_a?(::Hash)
            # 1. get all subclasses for the given type
            sub_classes=get_type_sub_classes(group,element_name)
            if sub_classes.key?(key)
              # 2.1 if there is a sub_class there has to be 
              #     a special class_name for the value 
              if sub_classes[key].key?(value['type']) 
                type_name=value['type']
                content += '<'+key+' class="'+sub_classes[key][type_name]['class_name']+'">'
                if value.key?('params')
                  value['params'].each do |inner_key,inner_value|
                    content += hash_to_xml(group,element_name,inner_key,inner_value)
                  end
                end
                content += '</'+key+'>'              
                already_processed = true
              end
            end

            if already_processed == false
              type_wrapper=get_type_wrapper(group,element_name)
              if type_wrapper.key?(key)
                # 2.1 if there is a sub_class there has to be 
                #     a special class_name for the value 
                if type_wrapper[key].key?('node_name') 
                  node_name=type_wrapper[key]['node_name']
                  content += '<'+node_name+'>'
                  value.each do |inner_key,inner_value|
                      content += hash_to_xml(group,element_name,inner_key,inner_value)
                  end
                  content += '</'+node_name+'>'              
                  already_processed = true
                end
              end
            end
          
          end

          if already_processed == false

            if value == ''
              content += "<"+key+"/>"
            else
              content += "<"+key+">"
              if value.is_a?(::Hash)          
                value.each do |inner_key,inner_value|
                  content += hash_to_xml(group,element_name,inner_key,inner_value)
                end
              else 
                str_value=value.to_s().strip().encode(:xml => :text)
                if !str_value.empty?
                  content += str_value
                end
              end
              content += "</"+key+">"
            end

          end
        end
        return content
      end

      def self.manage_default_parameter(group,name,user_options)
          # This function checks wether there is a file holding default parameters
          # for a given jenkins resource
          #
          # If default parameters are found, they will be merged with the given
          # parameters from the user
          fpath=File.join(File.dirname(__FILE__), '../../defaults/', group, name+'.json')
          if File.exist?(fpath)
            file = File.read(fpath)
            data_hash = JSON.parse(file)

            arr_opts = []
            if user_options.is_a?(::Array)
              user_options.each do |opt|
                arr_opts.push(data_hash.deep_merge(opt))
              end
              return arr_opts
            else
              return data_hash.deep_merge(user_options)
            end
          else
            return user_options
          end
        end

        def self.read_template_file(group,name,default_name)
            # check if there is a special template file for this builder
            tmpl_default_path = ''
            if default_name != ''
              tmpl_default_path=File.join(File.dirname(__FILE__), '../../templates/', default_name)
            end

            tmpl_path=File.join(File.dirname(__FILE__), '../../templates/',group,name)
            if File.exist?(tmpl_path)
              return File.read(tmpl_path)
            else
              return File.read(tmpl_default_path)
            end
          end


          def self.apply_scm_template(opts_in)
            if opts_in.empty?
              return ''
            end

            group="scm"
            # check if multiple-scms plugin has to be applied
            if opts_in.size == 1
              opts = opts_in[0]
              params=opts['params']
              element_name=opts['type']

              element_params=manage_default_parameter(group,element_name,params)

              tmpl_content = read_template_file(group,element_name+'.erb','scm.erb')
              template = ERB.new(tmpl_content,0,'>')
              output = template.result(binding)
            else
              # iterate over all scm items and apply template to it
              # we need to set multiple_scm_content variable since
              # multi_scm.erb will look for that variable
              multi_scm_content = ''
              opts_in.each do |opts|
                element_name=opts['type']
                params=opts['params']

                element_params=manage_default_parameter(group,element_name,params)

                tmpl_content = read_template_file(group,element_name+'.erb','scm.erb')
                template = ERB.new(tmpl_content,0,'>')
                multi_scm_content << template.result(binding)

              end

              element_name='MultiSCM'
              tmpl_content = read_template_file(group,'MultiSCM.erb','')
              template_multi= ERB.new(tmpl_content,0,'>')
              output=template_multi.result(binding)
            end

            return output
          end

          def self.apply_template(group,arr_elements)
            content = ''
            arr_elements.each do |element|
              element_name=element['type']

              params = {}
              if element.key?('params')
                params=element['params']
              end

              element_params=manage_default_parameter(group,element_name,params)
              template_file_content = read_template_file(group,element_name+'.erb',group+'.erb')


              template = ERB.new(template_file_content,0,'>')
              content += template.result(binding)
            end

            return content
          end

          def self.get_plugin_mapping_hash()
            fpath=File.join(File.dirname(__FILE__), '../../defaults/plugin_mapping.json')
            plugin_mapping = {}
            if File.exist?(fpath)
             file = File.read(fpath)
             plugin_mapping = JSON.parse(file)
           end
           return plugin_mapping
         end

         def self.is_type_referring_to_plugin(group,type_name)
          if $hash_plugin_mapping.key?(group) 
            if $hash_plugin_mapping[group].key?(type_name)
              return true
            end
          end
          return false
        end

        def self.get_plugin_name(group,type_name)
          if $hash_plugin_mapping.key?(group) 
            if $hash_plugin_mapping[group].key?(type_name)
              return $hash_plugin_mapping[group][type_name]['plugin_name']
            end
          end
          return type_name
        end

        def self.get_type_sub_classes(group,type_name)
          if $hash_plugin_mapping.key?(group) 
            if $hash_plugin_mapping[group].key?(type_name)
              if $hash_plugin_mapping[group][type_name].key?('sub_classes')
                return $hash_plugin_mapping[group][type_name]['sub_classes']
              end
            end
          end
          return {}
        end

        def self.get_type_wrapper(group,type_name)
          if $hash_plugin_mapping.key?(group) 
            if $hash_plugin_mapping[group].key?(type_name)
              if $hash_plugin_mapping[group][type_name].key?('type_wrapper')
                return $hash_plugin_mapping[group][type_name]['type_wrapper']
              end
            end
          end
          return {}
        end

        def self.get_plugin_version(group,type_name)
          plugin_name=get_plugin_name(group,type_name)

          if $available_plugins.key?(plugin_name) 
           return $available_plugins[plugin_name][:plugin_version]
         else
           raise "error: ws-cleanup plugin seems not to be installed"
         end
       end

       def self.get_version(plugin_name)
          if $available_plugins.key?(plugin_name) 
           return $available_plugins[plugin_name][:plugin_version]
         else
           raise "error: ws-cleanup plugin seems not to be installed"
         end
       end

       def self.get_type_class_name(group,type_name)
        if $hash_plugin_mapping.key?(group) 
          if $hash_plugin_mapping[group].key?(type_name)
            return $hash_plugin_mapping[group][type_name]['class_name']
          end
        end

        raise "type does not refer to any plugin"
      end

      def self.remove_empty_hashs(hash)
        hash.each do |key,value|
          if value == '' || value == '{}'
            hash.delete(key)
          elsif (value.is_a?(::Hash) || value.is_a?(::Array)) && value.empty?
            hash.delete(key)
          end
        end
        return hash
      end

      def self.create_xml(options)
            # read pluing mappings
            $hash_plugin_mapping = get_plugin_mapping_hash()
            $available_plugins = Puppet::Jenkins::Plugins::available
            
            user_options = options['user_options']

            # get description string
            description = ''
            if options.key?('description')
              description=options['description']
            end

            # this will create the actions part of the jenkins job
            job_actions_content = ''
            if options.key?('actions')
              job_actions_content=apply_scm_template(options['actions'])
            end

            # this will create the scm part of the jenkins job
            job_scm_content = ''
            if options.key?('scm')
              job_scm_content=apply_scm_template(options['scm'])
            end

            # this will create the properties part of the jenkins job
            job_properties_content = ''
            if options.key?('properties')
              job_properties_content=apply_template('properties',options['properties'])
            end

            # this will create the properties part of the jenkins job
            job_builders_content = ''
            if options.key?('builders')
              job_builders_content=apply_template('builders',options['builders'])
            end

            # this will create the publishers part of the jenkins job
            job_publishers_content = ''
            if options.key?('publishers')
              job_publishers_content=apply_template('publishers',options['publishers'])
            end
            
            # this will create the triggers part of the jenkins job
            job_triggers_content = ''
            if options.key?('triggers')
              job_triggers_content=apply_template('triggers',options['triggers'])
            end

            # this will create the build_wrappers part of the jenkins job
            job_build_wrappers_content = ''
            if options.key?('build_wrappers')
              job_build_wrappers_content=apply_template('build_wrappers',options['build_wrappers'])
            end

            # this will create the build_wrappers part of the jenkins job
            job_axes_content = ''
            if options.key?('axes')
              job_axes_content=apply_template('axes',options['axes'])
            end

            # now put everything together
            if options['template'].empty?
            case job_axes_content.empty?         
              when true
                element_params=manage_default_parameter('','jenkins-freestyle-job',options['params'])
                element_params=remove_empty_hashs(element_params)

                tmpl_job_content = File.read(File.join(File.dirname(__FILE__), '../../templates/', 'jenkins-freestyle-job.erb'))
              when false
                element_params=manage_default_parameter('','jenkins-matrix-job',options['params'])
                element_params=remove_empty_hashs(element_params)
                tmpl_job_content = File.read(File.join(File.dirname(__FILE__), '../../templates/', 'jenkins-matrix-job.erb'))
              end
            else
              tmpl_job_content = options['template']
            end
            
            job_content = ERB.new(tmpl_job_content,0,'>')

            # generate complete job content
            xml_content=job_content.result(binding) 

            # # now make it pretty
            # pretty_xml = ''
            # doc = REXML::Document.new(xml_content)
            # formatter = REXML::Formatters::Pretty.new

            # # Compact uses as little whitespace as possible
            # formatter.compact = true
            # formatter.write(doc,pretty_xml)
            # xml_content = pretty_xml

            return xml_content
          end
        end
      end
    end