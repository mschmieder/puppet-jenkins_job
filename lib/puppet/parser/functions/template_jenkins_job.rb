require File.join(File.dirname(__FILE__), '../../jenkins', 'job.rb')

module Puppet::Parser::Functions
  newfunction(:template_jenkins_job, :type => :rvalue) do |args|
    return Puppet::Jenkins::Job.create_xml(args[0])
  end
end