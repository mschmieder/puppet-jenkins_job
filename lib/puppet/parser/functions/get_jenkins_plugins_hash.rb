require File.join(File.dirname(__FILE__), '../../../../../jenkins/lib/puppet/jenkins', 'plugins.rb')

module Puppet::Parser::Functions
  newfunction(:get_jenkins_plugins_hash, :type => :rvalue) do |args|
    return Puppet::Jenkins::Plugins::available
  end
end